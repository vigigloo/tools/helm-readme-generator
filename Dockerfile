FROM oven/bun:1 AS builder

WORKDIR /opt/readme-generator

RUN apt update
RUN apt install -y curl

RUN curl -sSlLv -o /tmp/readme-generator-for-helm.tar.gz https://github.com/bitnami/readme-generator-for-helm/archive/refs/tags/2.6.0.tar.gz
RUN tar --strip-components=1 -xzf /tmp/readme-generator-for-helm.tar.gz

RUN bun install --frozen-lockfile --production

RUN bun build bin/index.js --compile --outfile readme-generator

FROM debian:12-slim

WORKDIR /app

ENV PATH=$PATH:/opt/readme-generator/bin

COPY --from=builder /opt/readme-generator/readme-generator /opt/readme-generator/bin/
COPY --from=builder /opt/readme-generator/config.json /opt/readme-generator/

RUN readme-generator --version

ENTRYPOINT ["readme-generator"]

CMD ["--help"]
